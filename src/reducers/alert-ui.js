const SHOW_ALERT = 'scratch-gui/alert-ui/SHOW_ALERT'
const CLOSE_ALERT = 'scratch-gui/alert-ui/CLOSE_ALERT' 

/*
    variant is basically and enum
    - success -> green alert
    - failure -> red alert
*/
const initialState = {
    alert_shown: false,
    variant: "success",
    header: '',
    description: ''
}

const reducer = function (state, action) {
    if (typeof state === 'undefined') state = initialState;
    switch(action.type) {
        case SHOW_ALERT:
            return Object.assign({}, state, {
                alert_shown: true,
                header: action.header,
                description: action.description,
                variant: action.variant
            });
        case CLOSE_ALERT:
            return Object.assign({}, state, {
                alert_shown: false,
                header: '',
                description: ''
            });
        default:
            return state
    }
}

const showAlert = (header, description, variant) => ({
    type: SHOW_ALERT,
    header,
    description,
    variant
})

const closeAlert = () => ({
    type: CLOSE_ALERT,
})

export {
    reducer as default,
    initialState as alertsUIInitialState,
    showAlert,
    closeAlert
}