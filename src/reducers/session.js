import checkSession from "../lib/smartcodeStuff/checkSession"

const START_LOGIN = 'START_LOGIN'
const END_LOGIN = 'END_LOGIN'
const START_LOGOUT = 'START_LOGOUT'
const END_LOGOUT = 'END_LOGOUT'
const SET_USERNAME = 'SET_USERNAME'

const initialState = {
    username: null,
    pending: false
};

const reducer = function (state, action) {
    if (typeof state === 'undefined') state = initialState;
    switch (action.type) {
        case START_LOGIN:
            return Object.assign({}, state, {
                pending: true
            });
        case END_LOGIN:
            if (action.username != null) {
                return Object.assign({}, state, {
                    username: action.username,
                    pending: false
                });
            } else {
                return Object.assign({}, state, {
                    pending: false
                });
            }
        case START_LOGOUT:
            return Object.assign({}, state, {
                pending: true
            });
        case END_LOGOUT:
            return Object.assign({}, state, {
                username: null,
                pending: false
            });
        case SET_USERNAME:
            return Object.assign({}, state, {
                username: action.username
            });
        default:
            return state;
    }
};

const startLogin = () => ({
    type: START_LOGIN
})

const endLogin = (username) => ({
    type: END_LOGIN,
    username
})

const startLogout = () => ({
    type: START_LOGOUT
})

const endLogout = () => ({
    type: END_LOGOUT
})

const setUsername = (username) => ({
    type: SET_USERNAME,
    username
})


export {
    reducer as default,
    initialState as sessionInitialState,
    startLogin,
    startLogout,
    endLogin,
    endLogout,
    setUsername
};
