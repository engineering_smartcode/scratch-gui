import React from "react"
import { Toast, Row, Button } from "react-bootstrap"
import { connect } from "react-redux"
import { closeAlert } from "../../reducers/alert-ui"
import styles from "./alert-toast.css"


const alertToast = props => {

    const {
        show,
        turnToastOff,
        variant,
        header,
        description
    } = props

    const toastClosed = () => {
        turnToastOff();
    }

    return (
        <Toast
            show={show}
            className={variant == 'success' ? styles.greenContainer : styles.redContainer}
            onClose={toastClosed}
            delay={6000}
            autohide
        >
            <Toast.Header closeButton={false} className={styles.toastHeader}>
                <strong>{header}</strong>
                <Button className={styles.toastButton} onClick={toastClosed}>
                    x
                </Button>
            </Toast.Header>
            <Toast.Body className={styles.toastBody}>
                {description}
            </Toast.Body>
        </Toast>
    )
}

const mapStateToProps = state => ({
    show: state.scratchGui.alert_ui.alert_shown,
    variant: state.scratchGui.alert_ui.variant,
    header: state.scratchGui.alert_ui.header,
    description: state.scratchGui.alert_ui.description
})

const mapDispatchToProps = dispatch => ({
    turnToastOff: () => dispatch(closeAlert())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(alertToast)
